<?php
include_once "vendor/autoload.php";
use Pondit\Institute\Student;
use Pondit\Institute\Subject;
use Pondit\Institute\Teacher;
use Pondit\Institute\Group;
use Pondit\Institute\Mark;

$student=new Student();
var_dump($student->name);

echo "<br>";
echo "<br>";

$subject=new Subject();
var_dump($subject->subject1,$subject->subject2,$subject->subject3);

echo "<br>";
echo "<br>";

$teacher=new Teacher();
var_dump($teacher->teacher);

echo "<br>";
echo "<br>";

$group=new Group();
var_dump($group);

echo "<br>";
echo "<br>";

$mark=new Mark();
var_dump($mark);